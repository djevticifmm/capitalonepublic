 google.maps.event.addDomListener(window, 'load', init);
    var map;
    function init() {
        var mapOptions = {
            center: new google.maps.LatLng(38.924523, -77.213169),
            zoom: 14,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.DEFAULT,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            },
            scaleControl: true,
            scrollwheel: false,
            panControl: true,
            streetViewControl: false,
            draggable : true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        /* "visibility": "off" */
	"visibility": "on"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "landscape.man_made",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#181717"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee" 
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }, {
		"visibility" : "on"
	}
    ]
  },
  {
    "featureType": "poi.business",
    "elementType": "geometry.fill",
    "stylers": [
      {
         "color": "#c3b8b7" 

      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
       "color": "#e5e5e5" 
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry.fill",
    "stylers": [
      {
       "color": "#c3b8b7" 

      },
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
       "color": "#ffffff"
	
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      },
      {
        "weight": 0.5
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#6d6d6d"
      },
      {
        "visibility": "on"
      },
      {
        "weight": 2
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#6d6d6d"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#6d6d6d"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text",
    "stylers": [
      {
        "color": "#6d6d6d"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "transit.station.rail",
    "stylers": [
      {
        "color": "#c3483a"
      },
      {
        "visibility": "on"
      },
      {
        "weight": 1.5
      }
    ]
  },
  {
    "featureType": "transit.station.rail",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#6d6d6d"
      },
      {
        "visibility": "on"
      },
      {
        "weight": 4
      }
    ]
  },
  {
    "featureType": "transit.station.rail",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "visibility": "on"
      }
	
    ]
  },
  {
    "featureType": "transit.station.rail",
    "elementType": "labels.icon",
    "stylers": [
      {
        "color": "#c3b8b7"  
       /*  "color": "#FF0000" */
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "transit.station.rail",
    "elementType": "labels.text",
    "stylers": [
      {
         "color": "#c3483a" 

      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        /* "color": "#c9c9c9" */
        "color": "#399cbd"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
],
        }
 
	var infowindow = new google.maps.InfoWindow({
          content: "<b>Capital One Center</b><br/>Corporate Headquarters"
        });

        var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            marker = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.924523,-77.213169),
              title: 'Capital One Center',
              cursor: 'hand',
              icon: 'https://uploads-ssl.webflow.com/5c5c721e8e26ba80f0c62b1c/5c61b70b45db006fb609aa65_capital-map-marker.png',
		content: "Test2"
            });
            map.setCenter(marker.getPosition());  

marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
          

var infowindow2 = new google.maps.InfoWindow({
          content: '<div jstcache="33" class="poi-info-window gm-style"> <div jstcache="2" style="display:none"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" class="address-line full-width" style="display:none" jsan="7.address-line,7.full-width"></div> </div> </div> <div jstcache="5" class="transit-container"> <div jstcache="10" class="transit-title" style="display:none"> <span jstcache="11"></span> </div> <div jstcache="12" class="transit-title"> <span jstcache="13">McLean Station</span> <div jstcache="14" class="transit-wheelchair-icon" style="display:none"></div> </div> <div jstcache="15" jsinstance="*0" class="transit-line-group" jsan="7.transit-line-group"> <div class="transit-line-group-vehicle-icons"> <img height="15" width="15" jstcache="16" src="https://maps.gstatic.com/mapfiles/transit/iw2/b/metro.png" jsinstance="*0"> </div> <div jstcache="17" class="transit-line-group-content"> <div jstcache="18" jsinstance="*0" class="transit-div-line-name"> <div jstcache="24" class="gm-transit-medium" jsan="7.gm-transit-medium"> <span jstcache="25" role="listitem" jsinstance="*0"><img height="15" width="15" jstcache="27" class="renderable-component-icon" style="display:none"><span jstcache="28"><div jstcache="29" class="renderable-component-color-box" style="display:none"></div><span jstcache="30" class="renderable-component-text-box renderable-component-bold" style="background-color:#a8a8a8;color:#000000" jsan="7.renderable-component-text-box,7.renderable-component-bold,5.background-color,5.color"><span jstcache="31" class="renderable-component-text-box-content" jsan="7.renderable-component-text-box-content">Silver</span></span><span jstcache="32" class="renderable-component-text renderable-component-bold" style="display:none" jsan="7.renderable-component-text,7.renderable-component-bold"></span></span></span> </div> </div> <div class="transit-clear-lines"></div> <div jstcache="19" class="transit-nlines-more-msg" style="display:none"> and <span jstcache="20"></span>&nbsp;more. </div> </div> </div> </div> <div class="view-link"> <a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=38.924523,-77.213169&amp;z=17&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3&amp;cid=11411816991360498489"> <span> View on Google Maps </span> </a> </div> </div>'
        });

      
            marker2 = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.92401, -77.21036),
              title: 'McLean Metro Station',
              cursor: 'hand',
              icon: 'https://demo.idamcloud.com/metrologo.jpg',
		
            });

marker2.addListener('click', function() {
          infowindow2.open(map, marker2);
        });
          

var infowindow3 = new google.maps.InfoWindow({
          content: '<div jstcache="33" class="poi-info-window gm-style"> <div jstcache="2" style="display:none"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" class="address-line full-width" style="display:none" jsan="7.address-line,7.full-width"></div> </div> </div> <div jstcache="5" class="transit-container"> <div jstcache="10" class="transit-title" style="display:none"> <span jstcache="11"></span> </div> <div jstcache="12" class="transit-title"> <span jstcache="13">Tysons Corner Station</span> <div jstcache="14" class="transit-wheelchair-icon" style="display:none"></div> </div> <div jstcache="15" jsinstance="*0" class="transit-line-group" jsan="7.transit-line-group"> <div class="transit-line-group-vehicle-icons"> <img height="15" width="15" jstcache="16" src="https://maps.gstatic.com/mapfiles/transit/iw2/b/metro.png" jsinstance="*0"> </div> <div jstcache="17" class="transit-line-group-content"> <div jstcache="18" jsinstance="*0" class="transit-div-line-name"> <div jstcache="24" class="gm-transit-medium" jsan="7.gm-transit-medium"> <span jstcache="25" role="listitem" jsinstance="*0"><img height="15" width="15" jstcache="27" class="renderable-component-icon" style="display:none"><span jstcache="28"><div jstcache="29" class="renderable-component-color-box" style="display:none"></div><span jstcache="30" class="renderable-component-text-box renderable-component-bold" style="background-color:#a8a8a8;color:#000000" jsan="7.renderable-component-text-box,7.renderable-component-bold,5.background-color,5.color"><span jstcache="31" class="renderable-component-text-box-content" jsan="7.renderable-component-text-box-content">Silver</span></span><span jstcache="32" class="renderable-component-text renderable-component-bold" style="display:none" jsan="7.renderable-component-text,7.renderable-component-bold"></span></span></span> </div> </div> <div class="transit-clear-lines"></div> <div jstcache="19" class="transit-nlines-more-msg" style="display:none"> and <span jstcache="20"></span>&nbsp;more. </div> </div> </div> </div> <div class="view-link"> <a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=38.924523,-77.213169&amp;z=14&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3&amp;cid=9029395741815402006"> <span> View on Google Maps </span> </a> </div> </div>'
        });

      
            marker3 = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.9203, -77.2222),
              title: 'Tysons Corner Metro Station',
              cursor: 'hand',
              icon: 'https://demo.idamcloud.com/metrologo.jpg',
		
            });

marker3.addListener('click', function() {
          infowindow3.open(map, marker3);
        });

 
var infowindow4 = new google.maps.InfoWindow({
          content: '<div jstcache="33" class="poi-info-window gm-style"> <div jstcache="2" style="display:none"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" class="address-line full-width" style="display:none" jsan="7.address-line,7.full-width"></div> </div> </div> <div jstcache="5" class="transit-container"> <div jstcache="10" class="transit-title" style="display:none"> <span jstcache="11"></span> </div> <div jstcache="12" class="transit-title"> <span jstcache="13">Greensboro Station</span> <div jstcache="14" class="transit-wheelchair-icon" style="display:none"></div> </div> <div jstcache="15" jsinstance="*0" class="transit-line-group" jsan="7.transit-line-group"> <div class="transit-line-group-vehicle-icons"> <img height="15" width="15" jstcache="16" src="https://maps.gstatic.com/mapfiles/transit/iw2/b/metro.png" jsinstance="*0"> </div> <div jstcache="17" class="transit-line-group-content"> <div jstcache="18" jsinstance="*0" class="transit-div-line-name"> <div jstcache="24" class="gm-transit-medium" jsan="7.gm-transit-medium"> <span jstcache="25" role="listitem" jsinstance="*0"><img height="15" width="15" jstcache="27" class="renderable-component-icon" style="display:none"><span jstcache="28"><div jstcache="29" class="renderable-component-color-box" style="display:none"></div><span jstcache="30" class="renderable-component-text-box renderable-component-bold" style="background-color:#a8a8a8;color:#000000" jsan="7.renderable-component-text-box,7.renderable-component-bold,5.background-color,5.color"><span jstcache="31" class="renderable-component-text-box-content" jsan="7.renderable-component-text-box-content">Silver</span></span><span jstcache="32" class="renderable-component-text renderable-component-bold" style="display:none" jsan="7.renderable-component-text,7.renderable-component-bold"></span></span></span> </div> </div> <div class="transit-clear-lines"></div> <div jstcache="19" class="transit-nlines-more-msg" style="display:none"> and <span jstcache="20"></span>&nbsp;more. </div> </div> </div> </div> <div class="view-link"> <a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=38.923388,-77.21642&amp;z=14&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3&amp;cid=6556338998176059786"> <span> View on Google Maps </span> </a> </div> </div>'
        });

      
            marker4 = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.9205, -77.2338),
              title: 'Greensboro Metro Station',
              cursor: 'hand',
              icon: 'https://demo.idamcloud.com/metrologo.jpg',
		
            });

marker4.addListener('click', function() {
          infowindow4.open(map, marker4);
        });


var infowindow5 = new google.maps.InfoWindow({
          content: '<div jstcache="33" class="poi-info-window gm-style"> <div jstcache="2" style="display:none"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" class="address-line full-width" style="display:none" jsan="7.address-line,7.full-width"></div> </div> </div> <div jstcache="5" class="transit-container"> <div jstcache="10" class="transit-title" style="display:none"> <span jstcache="11"></span> </div> <div jstcache="12" class="transit-title"> <span jstcache="13">Spring Hill Station</span> <div jstcache="14" class="transit-wheelchair-icon" style="display:none"></div> </div> <div jstcache="15" jsinstance="*0" class="transit-line-group" jsan="7.transit-line-group"> <div class="transit-line-group-vehicle-icons"> <img height="15" width="15" jstcache="16" src="https://maps.gstatic.com/mapfiles/transit/iw2/b/metro.png" jsinstance="*0"> </div> <div jstcache="17" class="transit-line-group-content"> <div jstcache="18" jsinstance="*0" class="transit-div-line-name"> <div jstcache="24" class="gm-transit-medium" jsan="7.gm-transit-medium"> <span jstcache="25" role="listitem" jsinstance="*0"><img height="15" width="15" jstcache="27" class="renderable-component-icon" style="display:none"><span jstcache="28"><div jstcache="29" class="renderable-component-color-box" style="display:none"></div><span jstcache="30" class="renderable-component-text-box renderable-component-bold" style="background-color:#a8a8a8;color:#000000" jsan="7.renderable-component-text-box,7.renderable-component-bold,5.background-color,5.color"><span jstcache="31" class="renderable-component-text-box-content" jsan="7.renderable-component-text-box-content">Silver</span></span><span jstcache="32" class="renderable-component-text renderable-component-bold" style="display:none" jsan="7.renderable-component-text,7.renderable-component-bold"></span></span></span> </div> </div> <div class="transit-clear-lines"></div> <div jstcache="19" class="transit-nlines-more-msg" style="display:none"> and <span jstcache="20"></span>&nbsp;more. </div> </div> </div> </div> <div class="view-link"> <a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=38.922211,-77.210594&amp;z=14&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3&amp;cid=6759813668250033936"> <span> View on Google Maps </span> </a> </div> </div>'
        });

      
            marker5 = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.9290, -77.2420),
              title: 'Spring Hill Metro Station',
              cursor: 'hand',
              icon: 'https://demo.idamcloud.com/metrologo.jpg',
		
            });

marker5.addListener('click', function() {
          infowindow5.open(map, marker5);
        });


var infowindow6 = new google.maps.InfoWindow({
          content: '<div jstcache="33" class="poi-info-window gm-style"> <div jstcache="2" style="display:none"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" class="address-line full-width" style="display:none" jsan="7.address-line,7.full-width"></div> </div> </div> <div jstcache="5" class="transit-container"> <div jstcache="10" class="transit-title" style="display:none"> <span jstcache="11"></span> </div> <div jstcache="12" class="transit-title"> <span jstcache="13">West Falls Church-VT/UVA</span> <div jstcache="14" class="transit-wheelchair-icon" style="display:none"></div> </div> <div jstcache="15" jsinstance="*0" class="transit-line-group" jsan="7.transit-line-group"> <div class="transit-line-group-vehicle-icons"> <img height="15" width="15" jstcache="16" src="https://maps.gstatic.com/mapfiles/transit/iw2/b/metro.png" jsinstance="*0"> </div> <div jstcache="17" class="transit-line-group-content"> <div jstcache="18" jsinstance="*0" class="transit-div-line-name"> <div jstcache="24" class="gm-transit-medium" jsan="7.gm-transit-medium"> <span jstcache="25" role="listitem" jsinstance="*0"><img height="15" width="15" jstcache="27" class="renderable-component-icon" style="display:none"><span jstcache="28"><div jstcache="29" class="renderable-component-color-box" style="display:none"></div><span jstcache="30" class="renderable-component-text-box renderable-component-bold" style="background-color:#f28d37;color:#000000" jsan="7.renderable-component-text-box,7.renderable-component-bold,5.background-color,5.color"><span jstcache="31" class="renderable-component-text-box-content" jsan="7.renderable-component-text-box-content">Orange</span></span><span jstcache="32" class="renderable-component-text renderable-component-bold" style="display:none" jsan="7.renderable-component-text,7.renderable-component-bold"></span></span></span> </div> </div> <div class="transit-clear-lines"></div> <div jstcache="19" class="transit-nlines-more-msg" style="display:none"> and <span jstcache="20"></span>&nbsp;more. </div> </div> </div> </div> <div class="view-link"> <a target="_blank" jstcache="6" href="https://maps.google.com/maps?ll=38.924523,-77.213169&amp;z=14&amp;t=m&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3&amp;cid=5194288884898263841"> <span> View on Google Maps </span> </a> </div> </div>'
        });

      
            marker6 = new google.maps.Marker({
              map:map,
              draggable:false,
              position: new google.maps.LatLng(38.9008, -77.1894),
              title: 'West Falls Church Metro Station',
              cursor: 'hand',
              icon: 'https://demo.idamcloud.com/metrologo.jpg',
		
            });

marker6.addListener('click', function() {
          infowindow6.open(map, marker6);
        });

        
}